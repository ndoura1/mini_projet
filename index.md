# Le Projet

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

               1- Presentation

Le Ludo est un `jeu de société` pour enfants et aussi bien pour les adultes, réunissant de deux à quatre joueurs. L'objectif principale de ce jeu est de faire parcourir ses quatre pions un tour complet du circuit dans le sens des aiguilles d'une montre et de les ramener à la maison avant les autres. En effet lors du parcours les joueurs peuvent s'attraper entre eux s'ils se coincent dans la même case.
Pour se déplacer les joueurs utilisent un Dee (valeur aléatoire) à tour de rôle.


   ![image_Ludo](image_Ludo.png)

