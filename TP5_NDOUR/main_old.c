/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "stdio.h"
#include <stdlib.h>
#include <time.h>
#include "image_de_fond_ludo.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define Lcase 272/15

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

DMA2D_HandleTypeDef hdma2d;

I2C_HandleTypeDef hi2c3;

LTDC_HandleTypeDef hltdc;

RNG_HandleTypeDef hrng;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart1;

SDRAM_HandleTypeDef hsdram1;

osThreadId Joueur_1Handle;
osThreadId Joueur_2Handle;
osThreadId Joueur_3Handle;
osThreadId Joueur_4Handle;
osThreadId piyon_joueur_1Handle;
osThreadId piyon_joueur_2Handle;
osThreadId piyon_joueur_3Handle;
osThreadId piyon_joueur_4Handle;
osThreadId ClotureHandle;
osThreadId Dee_1Handle;
osThreadId Dee_2Handle;
osThreadId Dee_3Handle;
osThreadId Dee_4Handle;
osMessageQId message_de_6Handle;
osMutexId Mutex01Handle;
/* USER CODE BEGIN PV */

typedef struct {
	uint16_t x;
	uint16_t y;
} coord;
coord coord_case[60];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C3_Init(void);
static void MX_LTDC_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_FMC_Init(void);
static void MX_DMA2D_Init(void);
static void MX_RNG_Init(void);
void Fonction_Joueur_1(void const *argument);
void Fonction_Joueur_2(void const *argument);
void Fonction_Joueur_3(void const *argument);
void Fonction_Joueur_4(void const *argument);
void Fonction_piyon_joueur_1(void const *argument);
void Fonction_piyon_joueur_2(void const *argument);
void Fonction_piyon_joueur_3(void const *argument);
void Fonction_piyon_joueur_4(void const *argument);
void Foction_cloture(void const *argument);
void Fonction_Dee_1(void const *argument);
void Fonction_Dee_2(void const *argument);
void Fonction_Dee_3(void const *argument);
void Fonction_Dee_4(void const *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t rxbuffer;
uint16_t compteur = 10;

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */
coord_case[0].x= Lcase/2;
coord_case[0].y= 136;
coord_case[1].x= Lcase/2;
coord_case[1].y= 136-Lcase;
coord_case[2].x= Lcase/2 + Lcase;
coord_case[2].y= 136-Lcase;
coord_case[3].x= Lcase/2 + 2*Lcase;
coord_case[3].y= 136-Lcase;
coord_case[4].x= Lcase/2 + 3*Lcase;
coord_case[4].y= 136-Lcase;
coord_case[5].x= Lcase/2 + 3*Lcase;
coord_case[5].y= 136-Lcase;
coord_case[6].x= Lcase/2 + 4*Lcase;
coord_case[6].y= 136-Lcase;
coord_case[7].x= Lcase/2 + 5*Lcase;
coord_case[7].y= 136-Lcase;
coord_case[8].x= Lcase/2 + 6*Lcase;
coord_case[8].y= 136-Lcase;
coord_case[9].x= Lcase/2 + 6*Lcase;
coord_case[9].y= 136-Lcase;
coord_case[10].x= Lcase/2 + 7*Lcase;
coord_case[10].y= 136-Lcase;
coord_case[11].x= Lcase/2 + 8*Lcase;
coord_case[11].y= 136-Lcase;
coord_case[12].x= Lcase/2 + 9*Lcase;
coord_case[12].y= 136-Lcase;
coord_case[13].x= Lcase/2 + 10*Lcase;
coord_case[13].y= 136-Lcase;
coord_case[14].x= Lcase/2 + 11*Lcase;
coord_case[14].y= 136-Lcase;
coord_case[15].x= Lcase/2 + 12*Lcase;
coord_case[15].y= 136-Lcase;


	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_I2C3_Init();
	MX_LTDC_Init();
	MX_RTC_Init();
	MX_TIM1_Init();
	MX_USART1_UART_Init();
	MX_FMC_Init();
	MX_DMA2D_Init();
	MX_RNG_Init();
	/* USER CODE BEGIN 2 */
	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
	BSP_LCD_LayerDefaultInit(1,
	LCD_FB_START_ADDRESS + BSP_LCD_GetXSize() * BSP_LCD_GetYSize() * 4);
	BSP_LCD_Clear(1);
	BSP_LCD_DisplayOn();
	BSP_LCD_SelectLayer(0);
	BSP_LCD_DrawBitmap(95, 0, (uint8_t*) image_de_fond_ludo_bmp);
	BSP_LCD_SelectLayer(1);
	BSP_LCD_Clear(0);
	BSP_LCD_SetFont(&Font24);
	BSP_LCD_SetTextColor(LCD_COLOR_MAGENTA);
	BSP_LCD_SetBackColor(LCD_COLOR_DARKCYAN);
	//  l’activation de l’interruption de réception d’un caractère (ou plus si besoin).
	HAL_UART_Receive_IT(&huart1, &rxbuffer, 1);
	// Activation du random pour generer un nombre aleatoire
	HAL_RNG_GenerateRandomNumber(&hrng, &rand);
	/* USER CODE END 2 */

	/* Create the mutex(es) */
	/* definition and creation of Mutex01 */
	osMutexDef(Mutex01);
	Mutex01Handle = osMutexCreate(osMutex(Mutex01));

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* Create the queue(s) */
	/* definition and creation of message_de_6 */
	osMessageQDef(message_de_6, 16, uint16_t);
	message_de_6Handle = osMessageCreate(osMessageQ(message_de_6), NULL);

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* definition and creation of Joueur_1 */
	osThreadDef(Joueur_1, Fonction_Joueur_1, osPriorityNormal, 0, 1024);
	Joueur_1Handle = osThreadCreate(osThread(Joueur_1), NULL);

	/* definition and creation of Joueur_2 */
	osThreadDef(Joueur_2, Fonction_Joueur_2, osPriorityNormal, 0, 1024);
	Joueur_2Handle = osThreadCreate(osThread(Joueur_2), NULL);

	/* definition and creation of Joueur_3 */
	osThreadDef(Joueur_3, Fonction_Joueur_3, osPriorityNormal, 0, 1024);
	Joueur_3Handle = osThreadCreate(osThread(Joueur_3), NULL);

	/* definition and creation of Joueur_4 */
	osThreadDef(Joueur_4, Fonction_Joueur_4, osPriorityNormal, 0, 1024);
	Joueur_4Handle = osThreadCreate(osThread(Joueur_4), NULL);

	/* definition and creation of piyon_joueur_1 */
	osThreadDef(piyon_joueur_1, Fonction_piyon_joueur_1, osPriorityHigh, 0,
			1024);
	piyon_joueur_1Handle = osThreadCreate(osThread(piyon_joueur_1), NULL);

	/* definition and creation of piyon_joueur_2 */
	osThreadDef(piyon_joueur_2, Fonction_piyon_joueur_2, osPriorityHigh, 0,
			1024);
	piyon_joueur_2Handle = osThreadCreate(osThread(piyon_joueur_2), NULL);

	/* definition and creation of piyon_joueur_3 */
	osThreadDef(piyon_joueur_3, Fonction_piyon_joueur_3, osPriorityHigh, 0,
			1024);
	piyon_joueur_3Handle = osThreadCreate(osThread(piyon_joueur_3), NULL);

	/* definition and creation of piyon_joueur_4 */
	osThreadDef(piyon_joueur_4, Fonction_piyon_joueur_4, osPriorityHigh, 0,
			1024);
	piyon_joueur_4Handle = osThreadCreate(osThread(piyon_joueur_4), NULL);

	/* definition and creation of Cloture */
	osThreadDef(Cloture, Foction_cloture, osPriorityNormal, 0, 1024);
	ClotureHandle = osThreadCreate(osThread(Cloture), NULL);

	/* definition and creation of Dee_1 */
	osThreadDef(Dee_1, Fonction_Dee_1, osPriorityLow, 0, 1024);
	Dee_1Handle = osThreadCreate(osThread(Dee_1), NULL);

	/* definition and creation of Dee_2 */
	osThreadDef(Dee_2, Fonction_Dee_2, osPriorityLow, 0, 1024);
	Dee_2Handle = osThreadCreate(osThread(Dee_2), NULL);

	/* definition and creation of Dee_3 */
	osThreadDef(Dee_3, Fonction_Dee_3, osPriorityLow, 0, 1024);
	Dee_3Handle = osThreadCreate(osThread(Dee_3), NULL);

	/* definition and creation of Dee_4 */
	osThreadDef(Dee_4, Fonction_Dee_4, osPriorityLow, 0, 128);
	Dee_4Handle = osThreadCreate(osThread(Dee_4), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = { 0 };

	/** Configure LSE Drive Capability
	 */
	HAL_PWR_EnableBkUpAccess();
	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI
			| RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 25;
	RCC_OscInitStruct.PLL.PLLN = 400;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Activate the Over-Drive mode
	 */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC
			| RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_I2C3
			| RCC_PERIPHCLK_CLK48;
	PeriphClkInitStruct.PLLSAI.PLLSAIN = 384;
	PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
	PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
	PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV8;
	PeriphClkInitStruct.PLLSAIDivQ = 1;
	PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_8;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
	PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLLSAIP;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief DMA2D Initialization Function
 * @param None
 * @retval None
 */
static void MX_DMA2D_Init(void) {

	/* USER CODE BEGIN DMA2D_Init 0 */

	/* USER CODE END DMA2D_Init 0 */

	/* USER CODE BEGIN DMA2D_Init 1 */

	/* USER CODE END DMA2D_Init 1 */
	hdma2d.Instance = DMA2D;
	hdma2d.Init.Mode = DMA2D_M2M;
	hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
	hdma2d.Init.OutputOffset = 0;
	hdma2d.LayerCfg[1].InputOffset = 0;
	hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
	hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
	hdma2d.LayerCfg[1].InputAlpha = 0;
	if (HAL_DMA2D_Init(&hdma2d) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN DMA2D_Init 2 */

	/* USER CODE END DMA2D_Init 2 */

}

/**
 * @brief I2C3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C3_Init(void) {

	/* USER CODE BEGIN I2C3_Init 0 */

	/* USER CODE END I2C3_Init 0 */

	/* USER CODE BEGIN I2C3_Init 1 */

	/* USER CODE END I2C3_Init 1 */
	hi2c3.Instance = I2C3;
	hi2c3.Init.Timing = 0x00C0EAFF;
	hi2c3.Init.OwnAddress1 = 0;
	hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c3.Init.OwnAddress2 = 0;
	hi2c3.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c3) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Analogue filter
	 */
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c3, I2C_ANALOGFILTER_ENABLE)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Configure Digital filter
	 */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c3, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C3_Init 2 */

	/* USER CODE END I2C3_Init 2 */

}

/**
 * @brief LTDC Initialization Function
 * @param None
 * @retval None
 */
static void MX_LTDC_Init(void) {

	/* USER CODE BEGIN LTDC_Init 0 */

	/* USER CODE END LTDC_Init 0 */

	LTDC_LayerCfgTypeDef pLayerCfg = { 0 };

	/* USER CODE BEGIN LTDC_Init 1 */

	/* USER CODE END LTDC_Init 1 */
	hltdc.Instance = LTDC;
	hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
	hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
	hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
	hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
	hltdc.Init.HorizontalSync = 40;
	hltdc.Init.VerticalSync = 9;
	hltdc.Init.AccumulatedHBP = 53;
	hltdc.Init.AccumulatedVBP = 11;
	hltdc.Init.AccumulatedActiveW = 533;
	hltdc.Init.AccumulatedActiveH = 283;
	hltdc.Init.TotalWidth = 565;
	hltdc.Init.TotalHeigh = 285;
	hltdc.Init.Backcolor.Blue = 0;
	hltdc.Init.Backcolor.Green = 0;
	hltdc.Init.Backcolor.Red = 0;
	if (HAL_LTDC_Init(&hltdc) != HAL_OK) {
		Error_Handler();
	}
	pLayerCfg.WindowX0 = 0;
	pLayerCfg.WindowX1 = 480;
	pLayerCfg.WindowY0 = 0;
	pLayerCfg.WindowY1 = 272;
	pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
	pLayerCfg.Alpha = 255;
	pLayerCfg.Alpha0 = 0;
	pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
	pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
	pLayerCfg.FBStartAdress = 0xC0000000;
	pLayerCfg.ImageWidth = 480;
	pLayerCfg.ImageHeight = 272;
	pLayerCfg.Backcolor.Blue = 0;
	pLayerCfg.Backcolor.Green = 0;
	pLayerCfg.Backcolor.Red = 0;
	if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN LTDC_Init 2 */

	/* USER CODE END LTDC_Init 2 */

}

/**
 * @brief RNG Initialization Function
 * @param None
 * @retval None
 */
static void MX_RNG_Init(void) {

	/* USER CODE BEGIN RNG_Init 0 */

	/* USER CODE END RNG_Init 0 */

	/* USER CODE BEGIN RNG_Init 1 */

	/* USER CODE END RNG_Init 1 */
	hrng.Instance = RNG;
	if (HAL_RNG_Init(&hrng) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN RNG_Init 2 */

	/* USER CODE END RNG_Init 2 */

}

/**
 * @brief RTC Initialization Function
 * @param None
 * @retval None
 */
static void MX_RTC_Init(void) {

	/* USER CODE BEGIN RTC_Init 0 */

	/* USER CODE END RTC_Init 0 */

	RTC_TimeTypeDef sTime = { 0 };
	RTC_DateTypeDef sDate = { 0 };
	RTC_AlarmTypeDef sAlarm = { 0 };

	/* USER CODE BEGIN RTC_Init 1 */

	/* USER CODE END RTC_Init 1 */
	/** Initialize RTC Only
	 */
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK) {
		Error_Handler();
	}

	/* USER CODE BEGIN Check_RTC_BKUP */

	/* USER CODE END Check_RTC_BKUP */

	/** Initialize RTC and set the Time and Date
	 */
	sTime.Hours = 0x0;
	sTime.Minutes = 0x0;
	sTime.Seconds = 0x0;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	sDate.WeekDay = RTC_WEEKDAY_MONDAY;
	sDate.Month = RTC_MONTH_JANUARY;
	sDate.Date = 0x1;
	sDate.Year = 0x0;
	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the Alarm A
	 */
	sAlarm.AlarmTime.Hours = 0x0;
	sAlarm.AlarmTime.Minutes = 0x0;
	sAlarm.AlarmTime.Seconds = 0x0;
	sAlarm.AlarmTime.SubSeconds = 0x0;
	sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
	sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
	sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
	sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
	sAlarm.AlarmDateWeekDay = 0x1;
	sAlarm.Alarm = RTC_ALARM_A;
	if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the Alarm B
	 */
	sAlarm.Alarm = RTC_ALARM_B;
	if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the TimeStamp
	 */
	if (HAL_RTCEx_SetTimeStamp(&hrtc, RTC_TIMESTAMPEDGE_RISING,
			RTC_TIMESTAMPPIN_POS1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN RTC_Init 2 */

	/* USER CODE END RTC_Init 2 */

}

/**
 * @brief TIM1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM1_Init(void) {

	/* USER CODE BEGIN TIM1_Init 0 */

	/* USER CODE END TIM1_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM1_Init 1 */

	/* USER CODE END TIM1_Init 1 */
	htim1.Instance = TIM1;
	htim1.Init.Prescaler = 0;
	htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim1.Init.Period = 65535;
	htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim1.Init.RepetitionCounter = 0;
	htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim1) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM1_Init 2 */

	/* USER CODE END TIM1_Init 2 */

}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void) {

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART1_Init 2 */

	/* USER CODE END USART1_Init 2 */

}

/* FMC initialization function */
static void MX_FMC_Init(void) {

	/* USER CODE BEGIN FMC_Init 0 */

	/* USER CODE END FMC_Init 0 */

	FMC_SDRAM_TimingTypeDef SdramTiming = { 0 };

	/* USER CODE BEGIN FMC_Init 1 */

	/* USER CODE END FMC_Init 1 */

	/** Perform the SDRAM1 memory initialization sequence
	 */
	hsdram1.Instance = FMC_SDRAM_DEVICE;
	/* hsdram1.Init */
	hsdram1.Init.SDBank = FMC_SDRAM_BANK1;
	hsdram1.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_8;
	hsdram1.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_12;
	hsdram1.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
	hsdram1.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
	hsdram1.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_1;
	hsdram1.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
	hsdram1.Init.SDClockPeriod = FMC_SDRAM_CLOCK_DISABLE;
	hsdram1.Init.ReadBurst = FMC_SDRAM_RBURST_DISABLE;
	hsdram1.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_0;
	/* SdramTiming */
	SdramTiming.LoadToActiveDelay = 16;
	SdramTiming.ExitSelfRefreshDelay = 16;
	SdramTiming.SelfRefreshTime = 16;
	SdramTiming.RowCycleDelay = 16;
	SdramTiming.WriteRecoveryTime = 16;
	SdramTiming.RPDelay = 16;
	SdramTiming.RCDDelay = 16;

	if (HAL_SDRAM_Init(&hsdram1, &SdramTiming) != HAL_OK) {
		Error_Handler();
	}

	/* USER CODE BEGIN FMC_Init 2 */

	/* USER CODE END FMC_Init 2 */
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();
	__HAL_RCC_GPIOJ_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOI_CLK_ENABLE();
	__HAL_RCC_GPIOK_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin,
			GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LCD_BL_CTRL_GPIO_Port, LCD_BL_CTRL_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LCD_DISP_GPIO_Port, LCD_DISP_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(EXT_RST_GPIO_Port, EXT_RST_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : USER_BP_Pin */
	GPIO_InitStruct.Pin = USER_BP_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(USER_BP_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : ARDUINO_SCL_D15_Pin ARDUINO_SDA_D14_Pin */
	GPIO_InitStruct.Pin = ARDUINO_SCL_D15_Pin | ARDUINO_SDA_D14_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : ULPI_D7_Pin ULPI_D6_Pin ULPI_D5_Pin ULPI_D2_Pin
	 ULPI_D1_Pin ULPI_D4_Pin */
	GPIO_InitStruct.Pin = ULPI_D7_Pin | ULPI_D6_Pin | ULPI_D5_Pin | ULPI_D2_Pin
			| ULPI_D1_Pin | ULPI_D4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : PB4 */
	GPIO_InitStruct.Pin = GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_VBUS_Pin */
	GPIO_InitStruct.Pin = OTG_FS_VBUS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(OTG_FS_VBUS_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : Audio_INT_Pin */
	GPIO_InitStruct.Pin = Audio_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(Audio_INT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
	GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PI2 */
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF3_TIM8;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	/*Configure GPIO pin : uSD_Detect_Pin */
	GPIO_InitStruct.Pin = uSD_Detect_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(uSD_Detect_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LCD_BL_CTRL_Pin */
	GPIO_InitStruct.Pin = LCD_BL_CTRL_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LCD_BL_CTRL_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
	GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : TP3_Pin NC2_Pin */
	GPIO_InitStruct.Pin = TP3_Pin | NC2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	/*Configure GPIO pins : LED1_Pin LCD_DISP_Pin */
	GPIO_InitStruct.Pin = LED1_Pin | LCD_DISP_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	/*Configure GPIO pin : PI0 */
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	/*Configure GPIO pin : User8_Pin */
	GPIO_InitStruct.Pin = User8_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(User8_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LCD_INT_Pin */
	GPIO_InitStruct.Pin = LCD_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(LCD_INT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PC7 PC6 */
	GPIO_InitStruct.Pin = GPIO_PIN_7 | GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : ULPI_NXT_Pin */
	GPIO_InitStruct.Pin = ULPI_NXT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(ULPI_NXT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PF7 */
	GPIO_InitStruct.Pin = GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF8_UART7;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	/*Configure GPIO pins : PF10 PF9 PF8 */
	GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_9 | GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	/*Configure GPIO pins : ULPI_STP_Pin ULPI_DIR_Pin */
	GPIO_InitStruct.Pin = ULPI_STP_Pin | ULPI_DIR_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : EXT_RST_Pin */
	GPIO_InitStruct.Pin = EXT_RST_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(EXT_RST_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : RMII_RXER_Pin */
	GPIO_InitStruct.Pin = RMII_RXER_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(RMII_RXER_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PA0 PA4 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : ULPI_CLK_Pin ULPI_D0_Pin */
	GPIO_InitStruct.Pin = ULPI_CLK_Pin | ULPI_D0_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB14 PB15 */
	GPIO_InitStruct.Pin = GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 8, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	//On relance l’interruption

}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_Fonction_Joueur_1 */
/**
 * @brief  Function implementing the Joueur_1 thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Joueur_1 */
void Fonction_Joueur_1(void const *argument) {
	/* USER CODE BEGIN 5 */
	uint16_t Message = 0;
	uint16_t compteur1 = 2, compteur2 = 5, i = 0, j = 0, declage1 = 0,
			declage2 = 0;
	/* Infinite loop */
	for (;;) {
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//		BSP_LCD_FillRect(38, 0, 130, 95);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
//		BSP_LCD_FillRect(68, 20, 70, 55);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1000);
//
//		osDelay(1);
//		while (j < compteur1) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//			BSP_LCD_FillRect(56, 96 + declage1, 18, 18);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage1 += 19;
//			j++;
//			osDelay(1);
//		}
//
//		while (i < compteur2) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//			BSP_LCD_FillRect(75 + declage2, 115, 22, 18);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage2 += 23;
//			i++;
//			osDelay(1);
//
//		}
		osDelay(1);
	}

	/* USER CODE END 5 */
}

/* USER CODE BEGIN Header_Fonction_Joueur_2 */
/**
 * @brief Function implementing the Joueur_2 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Joueur_2 */
void Fonction_Joueur_2(void const *argument) {
	/* USER CODE BEGIN Fonction_Joueur_2 */
	uint16_t compteur1 = 2, compteur2 = 5, i = 0, j = 0, declage1 = 0,
			declage2 = 0;
	/* Infinite loop */
	for (;;) {

//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//		BSP_LCD_FillRect(40, 153, 130, 180);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
//		BSP_LCD_FillRect(64, 178, 80, 70);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1000);
//		while (j < compteur1) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//			BSP_LCD_FillRect(171 + declage1, 235, 18, 18);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage1 += 19;
//			j++;
//			osDelay(1);
//		}
//
//		while (i < compteur2) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//			BSP_LCD_FillRect(190, 213 - declage2, 18, 20);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage2 += 21;
////			i++;
//			osDelay(1);
//		}
		osDelay(1);
	}
	/* USER CODE END Fonction_Joueur_2 */
}

/* USER CODE BEGIN Header_Fonction_Joueur_3 */
/**
 * @brief Function implementing the Joueur_3 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Joueur_3 */
void Fonction_Joueur_3(void const *argument) {
	/* USER CODE BEGIN Fonction_Joueur_3 */
	uint16_t compteur1 = 2, compteur2 = 5, i = 0, j = 0, declage1 = 0,
			declage2 = 0;
	/* Infinite loop */
	for (;;) {
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_RED);
//		BSP_LCD_FillRect(228, 0, 130, 95);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
//		BSP_LCD_FillRect(248, 20, 90, 55);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1000);
//
//		while (j < compteur1) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_RED);
//			BSP_LCD_FillRect(209 - declage1, 11, 18, 16);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage1 += 19;
//			j++;
//			osDelay(1);
//		}
//		while (i < compteur2) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_RED);
//			BSP_LCD_FillRect(190, 28 + declage2, 18, 16);
//
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage2 += 17;
//			i++;
//			osDelay(1);
//		}
		osDelay(1);
	}
	/* USER CODE END Fonction_Joueur_3 */
}

/* USER CODE BEGIN Header_Fonction_Joueur_4 */
/**
 * @brief Function implementing the Joueur_4 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Joueur_4 */
void Fonction_Joueur_4(void const *argument) {
	/* USER CODE BEGIN Fonction_Joueur_4 */
	uint16_t compteur1 = 2, compteur2 = 5, i = 0, j = 0, declage1 = 0,
			declage2 = 0;
	/* Infinite loop */
	for (;;) {

//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//		BSP_LCD_FillRect(227, 153, 130, 180);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
//		BSP_LCD_FillRect(248, 178, 90, 70);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1000);
//		while (j < compteur1) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//			BSP_LCD_FillRect(320, 134 - declage1, 18, 18);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage1 += 19;
//			j++;
//			osDelay(1);
//		}
//
//		while (i < compteur2) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//			BSP_LCD_FillRect(298 - declage2, 115, 21, 18);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage2 += 22;
//			i++;
//			osDelay(1);
//		}
		osDelay(1);
	}
	/* USER CODE END Fonction_Joueur_4 */
}

/* USER CODE BEGIN Header_Fonction_piyon_joueur_1 */
/**
 * @brief Function implementing the piyon_joueur_1 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_piyon_joueur_1 */
void Fonction_piyon_joueur_1(void const *argument) {
	/* USER CODE BEGIN Fonction_piyon_joueur_1 */
	uint16_t Message = 0;
	/* Infinite loop */
	for (;;) {
		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
		BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
		BSP_LCD_FillCircle(130, 54, 8);
		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//		BSP_LCD_FillCircle(80, 32, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//		BSP_LCD_FillCircle(128, 63, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//		BSP_LCD_FillCircle(128, 32, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		if (xQueueReceive(message_de_6Handle, &Message, portMAX_DELAY)) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
//			BSP_LCD_FillCircle(80, 63, 8);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			osDelay(10000);
//
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
//			BSP_LCD_FillCircle(85, 105, 8);
////			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			osDelay(1);
//
//		}
//		osDelay(1);
	}
	/* USER CODE END Fonction_piyon_joueur_1 */
}

/* USER CODE BEGIN Header_Fonction_piyon_joueur_2 */
/**
 * @brief Function implementing the piyon_joueur_2 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_piyon_joueur_2 */
void Fonction_piyon_joueur_2(void const *argument) {
	/* USER CODE BEGIN Fonction_piyon_joueur_2 */
	/* Infinite loop */
	for (;;) {
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//		BSP_LCD_FillCircle(80, 190, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//		BSP_LCD_FillCircle(80, 228, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//		BSP_LCD_FillCircle(128, 190, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//		BSP_LCD_FillCircle(128, 228, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
		osDelay(1);

	}
	/* USER CODE END Fonction_piyon_joueur_2 */
}

/* USER CODE BEGIN Header_Fonction_piyon_joueur_3 */
/**
 * @brief Function implementing the piyon_joueur_3 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_piyon_joueur_3 */
void Fonction_piyon_joueur_3(void const *argument) {
	/* USER CODE BEGIN Fonction_piyon_joueur_3 */
	/* Infinite loop */
	for (;;) {
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_RED);
//		BSP_LCD_FillCircle(263, 32, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_RED);
//		BSP_LCD_FillCircle(263, 60, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_RED);
//		BSP_LCD_FillCircle(320, 32, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_RED);
//		BSP_LCD_FillCircle(320, 60, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
		osDelay(1);

	}
	/* USER CODE END Fonction_piyon_joueur_3 */
}

/* USER CODE BEGIN Header_Fonction_piyon_joueur_4 */
/**
 * @brief Function implementing the piyon_joueur_4 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_piyon_joueur_4 */
void Fonction_piyon_joueur_4(void const *argument) {
	/* USER CODE BEGIN Fonction_piyon_joueur_4 */
	/* Infinite loop */
	for (;;) {
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//		BSP_LCD_FillCircle(265, 193, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//		BSP_LCD_FillCircle(265, 235, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//		BSP_LCD_FillCircle(320, 193, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
//		BSP_LCD_FillCircle(320, 235, 8);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
		osDelay(1);
	}
	/* USER CODE END Fonction_piyon_joueur_4 */
}

/* USER CODE BEGIN Header_Foction_cloture */
/**
 * @brief Function implementing the Cloture thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Foction_cloture */
void Foction_cloture(void const *argument) {
	/* USER CODE BEGIN Foction_cloture */
	uint16_t compteur1 = 3, compteur2 = 4, i = 0, j = 0, decalage1 = 0,
			declage2 = 0, decalage2 = 0, dclage2 = 0, dclage = 0;
	/* Infinite loop */
	for (;;) {

//		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//		BSP_LCD_SetTextColor( LCD_COLOR_BLACK);
//		BSP_LCD_DrawRect(56, 134, 18, 18);
//		BSP_LCD_DrawRect(208, 234, 18, 18);
//		BSP_LCD_DrawRect(209, 0, 18, 10);
//		BSP_LCD_DrawRect(170, 11, 18, 16);
//		BSP_LCD_DrawRect(319, 95, 18, 18);
//		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//		osDelay(1);
//
//		//En tête
//		while (j < compteur1) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor( LCD_COLOR_BLACK);
//			BSP_LCD_DrawRect(338, 133 - decalage1, 18, 18); // Jaune - rouge
//			BSP_LCD_DrawRect(37, 96 + decalage1, 18, 18);
//			BSP_LCD_DrawRect(171 + decalage1, 253, 18, 18);
//			BSP_LCD_DrawRect(209 - decalage1, 0, 18, 10);
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			decalage1 += 19;
//			j++;
//			osDelay(1);
//
//		}
//		//Couloir
//		while (i < compteur2) {
//			xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
//			BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
//			BSP_LCD_DrawRect(297 - dclage, 95, 21, 18); //rouge-jaune
//			BSP_LCD_DrawRect(297 - dclage, 133, 21, 18); //jaune-jaune
//			BSP_LCD_DrawRect(209, 28 + dclage2, 18, 16); //entre rouge-rouge
//			BSP_LCD_DrawRect(170, 27 + dclage2, 18, 16); //entre verte-rouge
//			BSP_LCD_DrawRect(74 + declage2, 96, 22, 18); //entre verte-verte
//			BSP_LCD_DrawRect(171, 214 - decalage2, 18, 20); //bleue-bleue
//			BSP_LCD_DrawRect(74 + declage2, 134, 22, 18); // entre bleue-verte
//			BSP_LCD_DrawRect(208, 213 - decalage2, 18, 20); //entre bleue -Jaune
//			xSemaphoreGive(Mutex01Handle); // mutex laisse la main
//			declage2 += 23;
//			decalage2 += 21;
//			dclage2 += 17;
//			dclage += 22;
//			i++;
//			osDelay(1);
//		}
//
		osDelay(1);
	}
	/* USER CODE END Foction_cloture */
}

/* USER CODE BEGIN Header_Fonction_Dee_1 */
/**
 * @brief Function implementing the Dee_1 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Dee_1 */
void Fonction_Dee_1(void const *argument) {
	/* USER CODE BEGIN Fonction_Dee_1 */
	uint8_t ecran_touche = 0, ecran_touche_old = 0, rect_present = 0;
	static uint16_t a = 0;
	uint16_t Message = 0;
	char text1[50] = { };
	static TS_StateTypeDef TS_State;
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
	srand(time(NULL));
	/* Infinite loop */
	for (;;) {
		BSP_TS_GetState(&TS_State);
		xSemaphoreTake(Mutex01Handle, portMAX_DELAY); //mutex prend la main
		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
		BSP_LCD_FillRect(0, 0, 40, 40);
		;
		BSP_LCD_FillRect(0, 232, 40, 40);
		BSP_LCD_FillRect(440, 0, 40, 40);
		BSP_LCD_FillRect(440, 232, 40, 40);
		xSemaphoreGive(Mutex01Handle); // mutex laisse la main
		ecran_touche = TS_State.touchDetected;
		if ((rect_present == 0) && (ecran_touche == 1)
				&& (ecran_touche_old == 0)) {
			if ((TS_State.touchX[0] >= 0) && (TS_State.touchX[0] <= 50)
					&& (TS_State.touchY[0] >= 0)
					&& (TS_State.touchY[0] <= 50)) {
				a = 1 + rand() % 6;
				sprintf(text1, "%d", a);
				xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
				BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
				BSP_LCD_DisplayStringAt(55, 10, (uint8_t*) text1, LEFT_MODE);
				xSemaphoreGive(Mutex01Handle);
				osDelay(1);
				if (a != 6) {
					sprintf(text1, "%d", a);
					xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
					BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
					BSP_LCD_DisplayStringAt(55, 10, (uint8_t*) text1,
							LEFT_MODE);
					xSemaphoreGive(Mutex01Handle);
				}
			}
		}
		ecran_touche_old = ecran_touche;

	}
	/* USER CODE END Fonction_Dee_1 */
}

/* USER CODE BEGIN Header_Fonction_Dee_2 */
/**
 * @brief Function implementing the Dee_2 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Dee_2 */
void Fonction_Dee_2(void const *argument) {
	/* USER CODE BEGIN Fonction_Dee_2 */
	uint8_t ecran_touche1 = 0, ecran_touche_old1 = 0, rect_present1 = 0;
	static uint16_t a1 = 0;
	uint16_t Message1 = 0;
	char text1[50] = { };
	static TS_StateTypeDef TS_State;
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
	srand(time(NULL));
	/* Infinite loop */
	for (;;) {
		BSP_TS_GetState(&TS_State);
		ecran_touche1 = TS_State.touchDetected;
		if ((rect_present1 == 0) && (ecran_touche1 == 1)
				&& (ecran_touche_old1 == 0)) {
			if ((TS_State.touchX[0] >= 0) && (TS_State.touchX[0] <= 50)
					&& (TS_State.touchY[0] >= 232)
					&& (TS_State.touchY[0] <= 272)) {
				a1 = 1 + rand() % 6;
				sprintf(text1, "%d", a1);
				xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
				BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
				BSP_LCD_DisplayStringAt(55, 240, (uint8_t*) text1, LEFT_MODE);
				xSemaphoreGive(Mutex01Handle);
				osDelay(1);
				if (a1 != 6) {
					sprintf(text1, "%d", a1);
					xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
					BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
					BSP_LCD_DisplayStringAt(55, 240, (uint8_t*) text1,
							LEFT_MODE);
					xSemaphoreGive(Mutex01Handle);
				}
			}
		}
		ecran_touche_old1 = ecran_touche1;
	}
	/* USER CODE END Fonction_Dee_2 */
}

/* USER CODE BEGIN Header_Fonction_Dee_3 */
/**
 * @brief Function implementing the Dee_3 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Dee_3 */
void Fonction_Dee_3(void const *argument) {
	/* USER CODE BEGIN Fonction_Dee_3 */
	uint8_t ecran_touche2 = 0, ecran_touche_old2 = 0, rect_present2 = 0;
	static uint16_t a2 = 0;
	uint16_t Message = 0;
	char text1[50] = { };
	static TS_StateTypeDef TS_State;
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
	srand(time(NULL));
	/* Infinite loop */
	for (;;) {
		BSP_TS_GetState(&TS_State);
		ecran_touche2 = TS_State.touchDetected;
		if ((rect_present2 == 0) && (ecran_touche2 == 1)
				&& (ecran_touche_old2 == 0)) {
			if ((TS_State.touchX[0] >= 430) && (TS_State.touchX[0] <= 480)
					&& (TS_State.touchY[0] >= 0)
					&& (TS_State.touchY[0] <= 50)) {
				a2 = 1 + rand() % 6;
				sprintf(text1, "%d", a2);
				xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
				BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
				BSP_LCD_DisplayStringAt(55, 10, (uint8_t*) text1, RIGHT_MODE);
				xSemaphoreGive(Mutex01Handle);
				osDelay(1);
				if (a2 != 6) {
					sprintf(text1, "%d", a2);
					xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
					BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
					BSP_LCD_DisplayStringAt(55, 10, (uint8_t*) text1,
							RIGHT_MODE);
					xSemaphoreGive(Mutex01Handle);
				}
			}
		}
		ecran_touche_old2 = ecran_touche2;

	}
	/* USER CODE END Fonction_Dee_3 */
}

/* USER CODE BEGIN Header_Fonction_Dee_4 */
/**
 * @brief Function implementing the Dee_4 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_Fonction_Dee_4 */
void Fonction_Dee_4(void const *argument) {
	uint8_t ecran_touche3 = 0, ecran_touche_old3 = 0, rect_present3 = 0;
	static uint16_t a3 = 0;
	uint16_t Message = 0;
	char text1[50] = { };
	static TS_StateTypeDef TS_State;
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
	srand(time(NULL));
	/* Infinite loop */
	for (;;) {
		BSP_TS_GetState(&TS_State);
		ecran_touche3 = TS_State.touchDetected;
		if ((rect_present3 == 0) && (ecran_touche3 == 1)
				&& (ecran_touche_old3 == 0)) {
			if ((TS_State.touchX[0] >= 430) && (TS_State.touchX[0] <= 480)
					&& (TS_State.touchY[0] >= 232)
					&& (TS_State.touchY[0] <= 272)) {
				a3 = 1 + rand() % 6;
				sprintf(text1, "%d", a3);
				xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
				BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
				BSP_LCD_DisplayStringAt(55, 240, (uint8_t*) text1, RIGHT_MODE);
				xSemaphoreGive(Mutex01Handle);
				osDelay(1);
				if (a3 != 6) {
					sprintf(text1, "%d", a3);
					xSemaphoreTake(Mutex01Handle, portMAX_DELAY);
					BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
					BSP_LCD_DisplayStringAt(55, 240, (uint8_t*) text1,
							RIGHT_MODE);
					xSemaphoreGive(Mutex01Handle);
				}
			}
		}
		ecran_touche_old3 = ecran_touche3;

	}
	/* USER CODE END Fonction_Dee_4 */
}

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM6 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM6) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */

	/* USER CODE END Callback 1 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
