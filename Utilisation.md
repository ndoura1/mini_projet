

## Règle du jeu de ludo Africain:

Chaque joueur choisit un cour parmis les 4 cour vert, jaune, blue et rouge et prend 4 pions de cette même couleur.
Pour pouvoir commencer à déplacer ses pions, le joueur doit obtenir un 6 avec le dé à coté de son cour. Le déplacement se fait à partir de la case départ de la couleur choisie.






![image_Ludo](IMG_2.png)